import { useEffect } from "react"
import DisplayCounties from "./components/DisplayCounties"
import Navbar from "./components/Navbar"
import { useAppDispatch, useAppSelector } from "./redux/hooks"
import { getCountries } from "./redux/countriesSlice"


function App() {

  const dispatch = useAppDispatch()
  const { darkMode } = useAppSelector(store => store.countries)

  useEffect(() => {
    dispatch(getCountries())
  }, [])

  return (
    <main className={`container-fluid bg-${darkMode ? 'dark bg-opacity-75' : 'light'}`}>
      <Navbar />
      <DisplayCounties />
    </main>
  )
}

export default App
