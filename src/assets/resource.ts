export const regionsArr: string[] = ['Africa', 'Americas', 'Asia', 'Europe', 'Oceania']
export const subRegionsArr: string[] = []
export const sortByArr: string[] = ['Name', 'Area', 'Population']
export const orderByArr: string[] = ['Ascending', 'Descending']