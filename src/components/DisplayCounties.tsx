import { useAppSelector } from "../redux/hooks"
import Country from "./Country"
import NotFound from "./NotFound"
import SearchBar from "./SearchBar"

const DisplayCounties = () => {

    const { countriesData, searchWord, region, subRegion,
        sortIs, order, isLoading, darkMode } = useAppSelector(store => store.countries)

    let filteredCountriesData: any[] = countriesData.filter((eachCountry: any) => {
        if (eachCountry.country.toLowerCase().includes(searchWord.toLowerCase()) &&
            eachCountry.region.includes(region) && (eachCountry.subRegion != undefined && eachCountry.subRegion.includes(subRegion))) {
            return eachCountry
        }
    })

    function sortingCountries() {
        if (order !== '' || sortIs !== '') {
            let name: string = ''
            sortIs == 'Name' ? name = 'country' : ''
            sortIs == 'Area' ? name = 'area' : ''
            sortIs == 'Population' ? name = 'population' : ''
            console.log(name)
            if (order == 'Ascending') {
                filteredCountriesData.sort((a, b) => a[name] > b[name] ? 1 : -1)
            } else if (order == 'Descending') {
                filteredCountriesData.sort((a, b) => a[name] > b[name] ? -1 : 1)
            } else {
                filteredCountriesData.sort((a, b) => a[name] > b[name] ? 1 : -1)
            }
        }
    }
    sortingCountries()

    return (
        <main className="row">
            <SearchBar />
            <section className="col-12 d-flex flex-wrap mt-4">
                {
                    filteredCountriesData.length > 0 ? filteredCountriesData.map((eachCountry: any) => {
                        return <Country key={countriesData.indexOf(eachCountry)} countryDetails={eachCountry} />
                    }) :
                        !isLoading && <div className="d-flex col-12 d-flex justify-content-center align-items-center">
                            <h1 className={`${darkMode ? 'text-light' : 'text-dark'}`}>
                                <NotFound />
                            </h1>
                        </div>
                }
                {
                    isLoading && <div className="d-flex col-12 d-flex justify-content-center align-items-center mt-5">
                        <div className="spinner-border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                }
            </section>
        </main>
    )
}

export default DisplayCounties