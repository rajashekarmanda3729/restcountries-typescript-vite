import { Component, ErrorInfo, ReactNode } from "react";
import Navbar from "./Navbar";

interface Props {
    children?: ReactNode;
}

interface State {
    hasError: boolean;
}

class ErrorBoundary extends Component<Props, State> {
    public state: State = {
        hasError: false
    };

    public static getDerivedStateFromError(_: Error): State {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        console.error("Uncaught error:", error, errorInfo);
    }

    public render() {
        if (this.state.hasError) {
            return <div className="container-fluid ">
                <div className="row">
                    <Navbar />
                    <div className="col-12 vh-100 d-flex border justify-content-center align-items-center">
                        <h1>Something went wrong...</h1>
                    </div>
                </div>
            </div>;
        }

        return this.props.children;
    }
}

export default ErrorBoundary;