import { regionsArr, sortByArr, orderByArr } from "../assets/resource"
import { useAppDispatch, useAppSelector } from "../redux/hooks"
import Filter from "./Filter"
import { onChangeName, onChangeOrder, onChangeRegion, onChangeSort, onChangeSubRegion } from "../redux/countriesSlice"

const SearchBar = () => {

    const dispatch = useAppDispatch()

    const { countriesData, region, subRegion, order,
        searchWord, sortIs, darkMode } = useAppSelector(store => store.countries)
    let filterSubRegionsArr: string[] = []
    function filterSubRegions(): void {
        countriesData.map((eachCountry: any) => {
            if (region !== '') {
                if (eachCountry.region == region && eachCountry.subRegion !== undefined) {
                    filterSubRegionsArr.push(eachCountry.subRegion)
                }
            } else {
                eachCountry.subRegion !== undefined && filterSubRegionsArr.push(eachCountry.subRegion)
            }
        })
    }
    filterSubRegions()
    const uniqueSubRegionsArr = new Set<string>(filterSubRegionsArr)
    filterSubRegionsArr = [...uniqueSubRegionsArr]

    return (
        <main className="col-12 d-flex justify-content-between ps-5 pe-5 mt-3">

            <input type="text" className={`rounded-4 col-2 col-md-4 p-1 ps-2 ${darkMode && ''}`} value={searchWord} onChange={(event) => dispatch(onChangeName(event.target.value))}
                placeholder="search country name" />

            <select name="region" value={region} className="rounded-4 p-1" onChange={(event) => dispatch(onChangeRegion(event.target.value))}>
                <option value="region">select Region</option>
                {
                    regionsArr.map(eachRegion => {
                        return <Filter key={eachRegion} name={eachRegion} />
                    })
                }
            </select>

            <select name="subRegion" value={subRegion} className="rounded-4 p-1" onChange={(event) => dispatch(onChangeSubRegion(event.target.value))}>
                <option value="subRegion">select SubRegion</option>
                {
                    filterSubRegionsArr.map(eachSubRegion => {
                        return <Filter name={eachSubRegion} key={Math.floor(Math.random() * 1000000)} />
                    })
                }
            </select>

            <select name="sortBy" value={sortIs} className="rounded-4 p-1" onChange={(event) => dispatch(onChangeSort(event.target.value))}>
                <option value="sortBy">select sort</option>
                {
                    sortByArr.map(eachFilter => {
                        return <Filter key={eachFilter} name={eachFilter} />
                    })
                }
            </select>

            <select name="orderBy" value={order} className="rounded-4 p-1" onChange={(event) => dispatch(onChangeOrder(event.target.value))}>
                <option value="orderBy">order By</option>
                {
                    orderByArr.map(eachOrder => {
                        return <Filter key={eachOrder} name={eachOrder} />
                    })
                }
            </select>

        </main>
    )
}

export default SearchBar