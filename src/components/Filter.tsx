
type SelectProps = {
    name: string
}

const Filter = (props: SelectProps) => {
    return (
        <option value={props.name}>{props.name}</option>
    )
}

export default Filter