import { BsMoonFill } from 'react-icons/bs'
import { useAppDispatch, useAppSelector } from '../redux/hooks'
import { darkModeSwitch } from '../redux/countriesSlice'

const Navbar = () => {

    const { darkMode } = useAppSelector(store => store.countries)
    const dispatch = useAppDispatch()

    return (
        <div className={`row shadow-lg rounded-4 p-3 pt-4 ${darkMode && 'bg-dark'}`}>
            <div className="col-12 d-flex justify-content-between">
                <h3 className={`text-${darkMode ? 'light' : 'dark'}`}> Where in the world ?</h3>
                <button className='bg-transparent border-0 d-flex col-1 justify-content-evenly align-items-center' onClick={() => dispatch(darkModeSwitch())}>
                    <BsMoonFill size={25} color={`${!darkMode ? 'black' : 'White'}`} />
                    <h3 className={`text-${darkMode ? 'light' : 'dark'}`}>{!darkMode ? 'Dark' : 'Light'}</h3>
                </button>
            </div>
        </div>
    )
}

export default Navbar