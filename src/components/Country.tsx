import { useAppSelector } from "../redux/hooks"

type CountryProps = {
    countryDetails: {
        country: string
        region: string
        area: number
        population: number
        url: string
        capital: string[] | string
        subRegion: string
    }
}

const Country = ({ countryDetails }: CountryProps) => {

    const { darkMode } = useAppSelector(store => store.countries)

    return (
        <div className="col-6 col-md-3 mt-5 d-flex justify-content-center">
            <div className={`card border-0 shadow-lg ${darkMode ? 'shadow-light' : 'shadow-light'}`} style={{ width: "18rem" }}>
                <img src={countryDetails.url} className="card-img-top" alt={countryDetails.country} />
                <div className={`card-body ${darkMode ? 'bg-dark text-light' : 'bg-light text-dark'}`}>
                    <h2 className=''>{countryDetails.country}</h2>
                    <h5 className="fs-5">capital : <span className={`fs-6 text-${darkMode ? 'warning' : 'success'}`}>{countryDetails.capital}</span></h5>
                    <h5 className="fs-5">Region : <span className={`fs-6 text-${darkMode ? 'warning' : 'success'}`}>{countryDetails.region}</span></h5>
                    <h5 className="fs-5">SubRegion : <span className={`fs-6 text-${darkMode ? 'warning' : 'success'}`}>{countryDetails.subRegion}</span></h5>
                    <h5 className="fs-5">Population : <span className={`fs-6 text-${darkMode ? 'warning' : 'success'}`}>{countryDetails.population}</span></h5>
                    <h5 className="fs-5">Area : <span className={`fs-6 text-${darkMode ? 'warning' : 'success'}`}>{countryDetails.area}</span></h5>
                </div>
            </div>


        </div>
    )
}

export default Country