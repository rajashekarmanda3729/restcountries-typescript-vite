
const NotFound = () => {
    return (
        <div className='d-flex flex-column vh-100 justify-content-center align-items-center'>
            <h1>No Data Found</h1>
            <img
                src="https://assets.ccbp.in/frontend/react-js/not-found-blog-img.png"
                alt="not found"
                className="w-100 rounded-5"
            />
        </div>
    )
}

export default NotFound
