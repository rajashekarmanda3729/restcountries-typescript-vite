import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { PayloadAction } from "@reduxjs/toolkit";

interface InitialState {
    region: string
    subRegion: string
    searchWord: string
    countriesData: {}[]
    order: string
    sortIs: string
    darkMode: boolean
    isLoading: boolean
}

const initialState: InitialState = {
    region: '',
    subRegion: '',
    searchWord: '',
    countriesData: [],
    order: '',
    sortIs: '',
    darkMode: false,
    isLoading: false
}

export const getCountries = createAsyncThunk(
    'countries/getCountries',
    async () => {
        try {
            const response = await axios('https://restcountries.com/v3.1/all')
            return response.data
        } catch (error) {
            console.log(error)
        }
    })

export const countriesSlice = createSlice({
    name: 'counties',
    initialState,
    reducers: {
        onChangeName: (state, action: PayloadAction<string>) => {
            state.searchWord = action.payload
        },
        onChangeRegion: (state, action: PayloadAction<string>) => {
            state.region = action.payload
        },
        onChangeSubRegion: (state, action: PayloadAction<string>) => {
            state.subRegion = action.payload
        },
        onChangeSort: (state, action: PayloadAction<string>) => {
            state.sortIs = action.payload
        },
        onChangeOrder: (state, action: PayloadAction<string>) => {
            state.order = action.payload
        },
        darkModeSwitch: (state) => {
            state.darkMode = !state.darkMode
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getCountries.pending, (state) => {
            state.isLoading = true
        })
        builder.addCase(getCountries.fulfilled, (state, action) => {
            state.isLoading = false

            type FilterArray = {
                country: string
                region: string
                area: number
                population: number
                url: string
                capital: string[] | string
                subRegion: string
            }
            const filterData: FilterArray[] = []

            action.payload.map((eachCountry: any) => {
                filterData.push({
                    country: eachCountry.name.common,
                    region: eachCountry.region,
                    area: eachCountry.area,
                    url: eachCountry.flags.png,
                    population: eachCountry.population,
                    capital: eachCountry.capital,
                    subRegion: eachCountry.subregion
                })
            })
            state.countriesData = filterData
        })
        builder.addCase(getCountries.rejected, (state) => {
            state.isLoading = false
        })
    }

})

export const { onChangeName, onChangeRegion, onChangeSubRegion,
    onChangeOrder, onChangeSort, darkModeSwitch } = countriesSlice.actions

export default countriesSlice.reducer